# tf-gce-sql
This module is for creating Cloud SQL databases, currently only second generation.

## Usage
Declare a module in your Terraform file, for example:
```
module "tf-gce-sql" {
  source = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-gce-sql.git?ref=v0.1"
  schemas = ["sakila"]
  db_user_host = "54.76.122.23"
  db_user_name = "mike"
  db_user_pass = "correcthorsebatterystaple"
}
```

Note the `ref=0.1` at the end of the `source` URL, this controls the version of this module that you import.

### Required Variables
* `db_user_host` - The host from which the user is connecting to the database. Can be hostname or IP address
* `db_user_name` - The login database user name
* `db_user_pass` - The password for the user
* `schemas` - List of database schemas to create

### Optional Variables
* `region` - (*default*: `europe-west1`)
* `database_version` - `MYSQL_5_6`|`MYSQL_5_7` (*default*: `MYSQL_5_7`)
* `machine_type` - Machine type as listed for [second generation instances](https://cloud.google.com/sql/docs/instance-settings#settings-2ndgen) (*default*: `db-f1-micro`)
* `backup_time` - `HH:MM` format time indicating when backup configuration starts (*default*: `01:00`)
* `inbound_cidrs` - List of CIDR blocks to allow connections from (*default*: `54.76.122.23/32` (our vpn))

### Outputs
* `ip_address` - IP address of created DB cluster
