resource "google_sql_database_instance" "master" {
#  name = "myinstance" DON'T NAME A DATABASE AS IT CAN'T BE RE-USED FOR 2 MONTHS!!!
  database_version = "${var.database_version}"
  region           = "${var.region}"
  count            = "${length(var.inbound_cidrs)}"
  project          = "${var.gcp_project}"

  settings = {
    tier = "${var.machine_type}"

    backup_configuration {
      binary_log_enabled = true
      enabled            = true

      /* If you don't specify this it defaults to 17:00 and every apply tf tries
       * to update it to ""
       */
      start_time = "${var.backup_time}"
    }

    ip_configuration {
      /* If you don't specify this it defaults to false and each run tries to
       * update it to false
       */
      ipv4_enabled = true
      require_ssl = "${var.ssl_enabled}"

      authorized_networks = [
        {
          value = "${var.inbound_cidrs[count.index]}"
        },
      ]
    }
  }
}

resource "google_sql_user" "root" {
  instance = "${google_sql_database_instance.master.name}"
  host     = "${var.db_user_host}"
  name     = "${var.db_user_name}"
  password = "${var.db_user_pass}"
}

resource "google_sql_database" "schemas" {
  count    = "${length(var.schemas)}"
  name     = "${var.schemas[count.index]}"
  instance = "${google_sql_database_instance.master.name}"
}
