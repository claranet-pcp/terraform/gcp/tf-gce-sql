variable "db_user_host" {
  description = "The host the user can connect from. Can be an IP address/CIDR block or hostname"
  type = "string"
}

variable "db_user_name" {
  description = "The username for the root instance account"
  type = "string"
}

variable "db_user_pass" {
  description = "The password for the root instance account"
  type = "string"
}

variable "gcp_project"  {
  description = "The GCP project in which to create this SQL instance"
  type = "string"
}

variable "region" {
  description = "The GCP region in which to create this SQL instance"
  type = "string"
  default = "europe-west1"
}

variable "database_version" {
  description = "The MySQL database version to use. Can be MYSQL_5_6, MYSQL_5_7 or POSTGRES_9_6"
  type = "string"
  default = "MYSQL_5_7"
}

variable "machine_type" {
  description = "The instance type to use for SQL instances"
  type = "string"
  default = "db-f1-micro"
}

variable "backup_time" {
  description = "Time of day to back the database instance up"
  type = "string"
  default = "01:00"
}

variable "schemas" {
  description = "List of schemas to create in this database instance"
  type = "list"
}

variable "inbound_cidrs" {
  description = "List of authorised networks to connect from, will create one database instance per element in this list"
  type    = "list"
  default = ["54.76.122.23/32"]
}

variable "ssl_enabled" {
  description = "Bool indicating whether SSL is enabled for SQL instances"
  type = "string"
  defult = "true"
}
